import '../styles/Header.css'

import { Link } from 'react-router-dom'
import { Container, Navbar, Nav } from 'react-bootstrap'

function Header() {
  return (
    <header className="header">
      <Navbar collapseOnSelect expand="xl" bg="default" variant="light">
        <Container fluid>
          <Navbar.Brand className="brand">
            <Link to="/">
              <img src="/static/logo.svg" alt="Logo" />
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse className="navigation" id="responsive-navbar-nav">
            <Nav className="m-auto">
              <Link className="nav-link" to="/">
                Home
              </Link>
              <Link className="nav-link" role="button" to="/contact">
                Contact
              </Link>

              <Link className="nav-link" role="button" to="/contact">
                Pricing
              </Link>
            </Nav>
            <Nav>
              <Link className="btn buy-btn" to="/numbers/buy">
                Buy Number <span>80% Off</span>
              </Link>
              <Link className="nav-link" to="/auth/login">
                Account
              </Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </header>
  )
}

export default Header
