import './App.css'

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import Header from './components/Header'
import Footer from './components/Footer'

import Home from './Home'
import Contact from './pages/Contact'

import Container from 'react-bootstrap/Container'

function App() {
  return (
    <div className="app">
      <Router>
        <Header />
        <Container fluid>
          <Switch>
            <Route path="/Contact">
              <Contact />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </Container>
        <Footer />
      </Router>
    </div>
  )
}

export default App
